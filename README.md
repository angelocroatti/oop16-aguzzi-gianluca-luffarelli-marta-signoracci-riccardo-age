## Synopsis

**Age of quiz** is a videogame rpg-based. You advance in the story answering some question.

## Motivation

**Age of quiz** is born to try to create a very basic but fun videogame. 

## Contributors
* **Aguzzi Gianluca**
* **Marta Luffarelli**
* **Riccardo Signoracci**